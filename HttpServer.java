package test;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.util.ArrayList;

public class HttpServer implements Runnable {

    ServerSocket serverSocket;//服务器Socket

    public static int PORT = 80;//标准HTTP端口

    public String encoding = "GBK";

    public HttpServer() {
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Thread(this).start();
        System.out.println("HTTP服务器运行,端口:" + PORT);
    }

    @Override
    public void run() {
        while (true) {
            try {
                Socket client = serverSocket.accept();
                if (client != null) {
                    try {
                        InputStream is = client.getInputStream();
                        String line = readLine(is, 0);
                        String resource = line.substring(line.indexOf('/'), line
                                .lastIndexOf('/') - 5);
                        resource = URLDecoder.decode(resource, encoding);
                        System.out.println("URI是:" + resource);
                        PrintWriter out = new PrintWriter(client.getOutputStream(),
                                true);
                        String s = excute(resource);
                        out.println(s);

                        out.close();

                        try {
                            client.close();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    } catch (Exception e) {
                        System.out.println("HTTP服务器错误:" + e.getLocalizedMessage());
                    }
                }
            } catch (Exception e) {
                System.out.println("HTTP服务器错误:" + e.getLocalizedMessage());
            }
        }
    }

    private String excute(String resource) {
        String result = null;
        if(resource.contains("add")){
            String[] split = resource.split("\\?");
            result=split[1];
            String[] split1 = result.split("&");
            int a = Integer.parseInt(split1[0].substring(2, split1[0].length()));
            int b = Integer.parseInt(split1[1].substring(2, split1[1].length()));
            result=String.valueOf(a+b);
        }else if(resource.contains("mult")){
            String[] split = resource.split("\\?");
            result=split[1];
            String[] split1 = result.split("&");
            int a = Integer.parseInt(split1[0].substring(2, split1[0].length()));
            int b = Integer.parseInt(split1[1].substring(2, split1[1].length()));
            result=String.valueOf(a*b);
        }else{
            result="请求不合法，请核对重试";
        }
        return result;
    }

    private String readLine(InputStream is, int contentLe) throws IOException {
        ArrayList lineByteList = new ArrayList();
        byte readByte;
        int total = 0;
        if (contentLe != 0) {
            do {
                readByte = (byte) is.read();
                lineByteList.add(Byte.valueOf(readByte));
                total++;
            } while (total < contentLe);
        } else {
            do {
                readByte = (byte) is.read();
                lineByteList.add(Byte.valueOf(readByte));
            } while (readByte != 10);
        }

        byte[] tmpByteArr = new byte[lineByteList.size()];
        for (int i = 0; i < lineByteList.size(); i++) {
            tmpByteArr[i] = ((Byte) lineByteList.get(i)).byteValue();
        }
        lineByteList.clear();

        String tmpStr = new String(tmpByteArr, encoding);

        return tmpStr;
    }


    public static void main(String[] args) {
        PORT = 80;
        new HttpServer();
    }
}
